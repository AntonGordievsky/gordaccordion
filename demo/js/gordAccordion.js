/**
 * @author   Antonio Gordievskiy
 * @module   jQuery module
 * @name     gordAccordion
 * @link     https://bitbucket.org/AntonGordievsky/gordaccordion
 * @throws   If the containers for component is not specified,
             an exception is {"Container is not defined"}
 */

;(function ( $ ) {

    $.fn.gordAccordion = function (options) {
        var settings = $.extend({
            containers: $(this),
            speed     : 200
        }, options );

        if ( settings.containers.length == 0 ) {
            console.warn( 'Container is not defined' );
        };

        $(settings.containers).each(function() {
            var previousActiveElement = $(this).find("div:first"),
                previousActiveCaption = $(this).find("h3:first")
                .addClass('active');

            $(this).find("div").not(":first").slideDown(0).slideUp(0);

            $(this).find("h3").click(function(event) {
                if ($(event.target)[0] == previousActiveCaption[0]) {
                    return;
                };

                $(previousActiveElement).slideUp(settings.speed);
                $($(event.target).next()).slideDown(settings.speed);
                previousActiveElement = $(event.target).next();
                $(previousActiveCaption).removeClass('active');
                $(event.target).addClass('active');
                previousActiveCaption = $(event.target);

            })
        });

        return this;
    }
})( jQuery );
