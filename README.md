## gordAccordion ##

This component creates an accordion with an adjustable playback speed.

[Demo](https://gordievskiy.com/lab/gordAccordion)

## How do I get set up? ##

**First, make sure you are using valid DOCTYPE and the jQuery library.**

**Include necessary JS file:**
    
```
#!html

<script src="js/gordAccordion.js" type="text/javascript"></script>
```

**Add CSS file:**

```
#!html

<link href="css/gordAccordion.css" rel="stylesheet">
```

You can specify a custom icon or rotation speed right in CSS:

```
#!html

.accordion h3::before {
    /*  Here you can set another icon URL:  */
    content: url("../img/accordion-icon.png");

    /*  Duration of icon rotation: */
    -webkit-transition: 0.2s;
    transition: 0.2s;
}
```

*In these styles, arrows are created using pseudo-classes "before" and "after" of H3 element. They are inverted by adding class "active" to H3. The effect duration is specified in the pseudo-elements styles.*

**Create an HTML structure:**

```
#!html

<div class="accordion">
    <h3>Section 1</h3>
    <div>
        <p>Lorem ipsum dolor sit amet</p>
    </div>
</div>
```
*Follow this structure of headings and text and your accordion will work flawlessly!*

**Fire plugin using jQuery selector.**

```
#!javascript

$(selector).gordAccordion();
```
This basic sample uses default speed: 200 ms.

**You can set the custom speed:**

```
#!html

$(".accordion").gordAccordion({
    speed: 300
});
```

*You can initialize instances of the effect one at a time, or you can specify settings for multiple instances at once, if they're the same.*